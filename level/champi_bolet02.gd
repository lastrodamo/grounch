extends Spatial

onready var soundfx = get_node("sfx_gem_b2")
export var collision_count = 0 #Your counter.

func _ready():
	pass

func _on_Area_Bolet02_body_entered(body):
	if body is KinematicBody:
		get_node(".").hide()
		collision_count += 1
		soundfx.play()
	pass # Replace with function body.

extends Spatial

onready var animation = get_node("AnimationPlayer")
onready var soundfx = get_node("sfx_gem_orange")
export var collision_count_orange = 0 #Your counter.

func _ready():
	animation.get_animation("Drop_move_blue").set_loop(true)
	animation.play("Drop_move_blue", 1, 0.83, true)
	pass

func _on_Area_Drop_Lantern_orange_body_entered(body):
	if body is KinematicBody:
		get_node(".").hide()
		collision_count_orange += 1
		soundfx.play()
	pass # Replace with function body.

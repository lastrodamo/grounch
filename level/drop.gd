extends Spatial

onready var animation = get_node("AnimationPlayer")
onready var soundfx = get_node("sfx_gem")
export var collision_count = 0 #Your counter.

func _ready():
	animation.get_animation("Drop_move_blue").set_loop(true)
	animation.play("Drop_move_blue", 1, 1, true)
	pass

func _on_Area_body_entered(body):
	if body is KinematicBody:
		get_node(".").hide()
		collision_count += 1
		soundfx.play()
	pass # Replace with function body.


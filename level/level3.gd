extends Spatial

# Timer
var time_start = 0
var time_now = 0

# Wait time Game Over
var elapsed_seconds = 0
var max_seconds = 1.5

# Wait time psyche
var elapsed_seconds_p = 0
var max_seconds_p = 8.0

# Eat
onready var animation_crapaud = get_node("Crapaud/crapaud/AnimationPlayer")

# Hue
onready var hue_timer = 0
onready var speed = 300 #degrees per second 60

# Firefly, Frog
#onready var luciole = get_node("Path/PathFollow/firefly_blue")
onready var crapaud = get_node("Crapaud")

# pos firefly
export var pos_f = Vector3()

# bullet
#var bullet = preload("res://vfx/bullet.tscn").instance()
onready var bullet = get_node("bullet")

# settings
onready var vol = preload("res://settings/settings.tscn")

# Settings Panel
func _ready():
	if (settings.gi_quality == settings.GI_QUALITY_HIGH):
		ProjectSettings["rendering/quality/voxel_cone_tracing/high_quality"]=true
	elif (settings.gi_quality == settings.GI_QUALITY_LOW):
		ProjectSettings["rendering/quality/voxel_cone_tracing/high_quality"]=false
	else:
		$GIProbe.hide()
		$refprobes.show()
		
	if (settings.aa_quality == settings.AA_8X):
		get_node("/root").msaa = Viewport.MSAA_8X
	elif (settings.aa_quality == settings.AA_4X):
		get_node("/root").msaa = Viewport.MSAA_4X
	elif (settings.aa_quality == settings.AA_2X):
		get_node("/root").msaa = Viewport.MSAA_2X
	else:
		get_node("/root").msaa = Viewport.MSAA_DISABLED
		
	if (settings.ssao_quality == settings.SSAO_QUALITY_HIGH):
		pass
	elif (settings.ssao_quality == settings.SSAO_QUALITY_LOW):
		pass
	else:
		$WorldEnvironment.environment.ssao_enabled = false
		
	if (settings.resolution == settings.RESOLUTION_NATIVE):
		pass
	elif (settings.ssao_quality == settings.RESOLUTION_1080):
		var minsize=Vector2( OS.window_size.x * 1080 / OS.window_size.y, 1080.0)
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,SceneTree.STRETCH_ASPECT_KEEP_HEIGHT,minsize)
	elif (settings.ssao_quality == settings.RESOLUTION_720):
		var minsize=Vector2( OS.window_size.x * 720 / OS.window_size.y, 720.0)
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,SceneTree.STRETCH_ASPECT_KEEP_HEIGHT,minsize)
	elif (settings.ssao_quality == settings.RESOLUTION_576):
		var minsize=Vector2( OS.window_size.x * 576 / OS.window_size.y, 576.0)
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,SceneTree.STRETCH_ASPECT_KEEP_HEIGHT,minsize)

# timer
	time_start = OS.get_unix_time()
	set_process(true)

# game design
	get_node("Game_design_level3").show()

##### animation pistons
	var anim_piston = get_node("pistons/AnimationPlayer").get_animation("Pistons")
	anim_piston.set_loop(true)
	get_node("pistons/AnimationPlayer").play("Pistons")

##### animation troll door
	var anim_troll_door = get_node("troll_door/AnimationPlayer").get_animation("Troll_door")
	anim_troll_door.set_loop(false)
	get_node("troll_door/AnimationPlayer").stop()

##### animation ventilos
	var anim_ventilos = get_node("ventilos/AnimationPlayer").get_animation("Ventilos")
	anim_ventilos.set_loop(true)
	get_node("ventilos/AnimationPlayer").play("Ventilos")

##### animation vers de terre
	var anim_vers = get_node("vers_de_terre/AnimationPlayer").get_animation("Danse")
	anim_vers.set_loop(true)
	get_node("vers_de_terre/AnimationPlayer").play("Danse")

	var anim_vers2 = get_node("vers_de_terre2/AnimationPlayer").get_animation("Danse")
	anim_vers2.set_loop(true)
	get_node("vers_de_terre2/AnimationPlayer").play("Danse")

func _process(delta):
# Return to menu
	if Input.is_action_just_pressed("ui_cancel") or Input.is_action_just_pressed("p"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		var go_menu_pause = get_node("Menu_pause")
		go_menu_pause.show()

##### Game over #####
	var game_over = get_node("Crapaud")
	#var pos_init = Vector3(0,0,0)
	#print("G : ", game_over.G)

##### Timer #####
	if game_over.G <= -6.81:
		elapsed_seconds += delta
		if elapsed_seconds > max_seconds:
			elapsed_seconds = 1
			var death = get_node("Crapaud/crapaud/AnimationPlayer")
			death.get_animation("Fall").set_loop(true)
			death.play("Fall", 1, 1, true)
	pass

func _physics_process(delta):
	if Input.is_action_pressed("e") or Input.is_action_pressed("LMB"):
		pos_f = get_node("Path/PathFollow").get_translation()
		var pos_ff = Vector3(pos_f.x, 1, pos_f.z)
		#print("Translation : ", pos_ff)
		var crap = crapaud.get_node("MeshInstance")
		#var pos_c = Vector3()
		var init_pose = crapaud.get_translation()
		crap.set_translation(pos_ff - init_pose)

# Compteur de champignons et Goutes
	var c0 = get_node("Drop").collision_count
	var c1 = get_node("Drop2").collision_count1
	var c2 = get_node("Drop3").collision_count2
	var c3 = get_node("Drop4").collision_count3
	var cb = get_node("Drop_Lantern_blue").collision_count_blue
	var ck = get_node("Key").collision_k_count
	var ccb0 = get_node("Bolet00").collision_count
	var ccb1 = get_node("Bolet01").collision_count
	var ccb2 = get_node("Bolet02").collision_count
	var ccc1 = get_node("Coprin01").collision_count
	var cck = get_node("Key").collision_k_count

	var count = c0 + c1 + c2 + c3
	var ccb = ccb0 + ccb1 + ccb2
	var ccc = ccc1
	#var ccp = ccp2
	
	#print("count = ", count)
	#print("ccb = ", ccb)
	#print("ccp = ", ccp)
	
#### Ouverture de la lanterne bleue
	#var lantern_blue = get_node("Lantern_blue/AnimationPlayer")
	var firefly_blue = get_node("Path/PathFollow/firefly_blue")
	if cb == 1:
		#lantern_blue = lantern_blue.play("open_door_blue", 1, 0.3)
		firefly_blue.get_parent().set_offset(firefly_blue.get_parent().get_offset() + (5*delta))
		get_node("Path/PathFollow/A2udioStreamPlayer3D").play(true)

	var gd_mauve = get_node("Game_design_level3/ProgressBar_perle_mauve")
	var gd_bleue = get_node("Game_design_level3/ProgressBar_perle_bleue")
#	var gd_orange = get_node("Game_design/ProgressBar_perle_orange")
	var gd_bolet = get_node("Game_design_level3/ProgressBar_champi_bolet")
	var gd_coprin = get_node("Game_design_level3/ProgressBar_champi_coprin")
	var gd_key = get_node("Game_design_level3/ProgressBar_key")
	
	#warning-ignore:unused_variable
	var pb_m
	#warning-ignore:unused_variable
	var pb_b
	#warning-ignore:unused_variable
	var pb_o
	#warning-ignore:unused_variable
	var pb_cb
	#warning-ignore:unused_variable
	var pb_cc
	#warning-ignore:unused_variable
	var pb_ck
	
	pb_m = gd_mauve.set_value(count*25)
	pb_b = gd_bleue.set_value(cb*100)
	#pb_o = gd_orange.set_value(co*100)
	pb_cb = gd_bolet.set_value(ccb*33.4)
	pb_cc = gd_coprin.set_value(ccc*100)
	pb_ck = gd_key.set_value(cck*100)

	var base_color = Color(0.8, 0.8, 0.8, 1.0)
	var crapaud_shading = get_node("Crapaud/crapaud/Armature_crapaud/Skeleton/Crapaud").get_mesh().surface_get_material(0)

	if c0 >= 1:
		var areac0 = get_node("Area_Drop/CollisionShape")
		areac0.set_disabled(true)
	if c1 >= 1:
		var areac1 = get_node("Area_Drop2/CollisionShape")
		areac1.set_disabled(true)
	if c2 >= 1:
		var areac2 = get_node("Area_Drop3/CollisionShape")
		areac2.set_disabled(true)
	if c3 >= 1:
		var areac3 = get_node("Area_Drop4/CollisionShape")
		areac3.set_disabled(true)
	if cb >= 1:
		var areacb = get_node("Area_Drop_Lantern_blue/CollisionShape")
		areacb.set_disabled(true)
	#if co >= 1:
	#	var areaco = get_node("Area_Drop_Lantern_orange/CollisionShape")
	#	areaco.set_disabled(true)
	if ccb0 >= 1:
		var areab0 = get_node("Area_Bolet00/CollisionShape")
		areab0.set_disabled(true)
	if ccb1 >= 1:
		var areab1 = get_node("Area_Bolet01/CollisionShape")
		areab1.set_disabled(true)
	if ccb2 >= 1:
		var areab2 = get_node("Area_Bolet02/CollisionShape")
		areab2.set_disabled(true)
	if ccc1 >= 1:
		var areap1 = get_node("Area_Coprin01/CollisionShape")
		areap1.set_disabled(true)

	if (count == 4 and ccb == 3 and ccc == 1 and cb == 1):
		get_node("Vortex").show()
		get_node("Area_vortex").show()
		get_node("Area_vortex/CollisionShape").set_disabled(false)
		#var mouse_pos = get_viewport().get_mouse_position()
		
func _on_Area_vortex_body_entered(body):
	if body is KinematicBody:
#warning-ignore:return_value_discarded
		get_tree().change_scene("res://level/panel_levels/panel_level3.tscn")
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	pass # Replace with function body.
		
		
func _on_sfx_gem_finished():
	get_node("Drop/sfx_gem").stream_paused = true
func _on_sfx_gem2_finished():
	get_node("Drop2/sfx_gem2").stream_paused = true
func _on_sfx_gem3_finished():
	get_node("Drop3/sfx_gem3").stream_paused = true
func _on_sfx_gem4_finished():
	get_node("Drop4/sfx_gem4").stream_paused = true
#func _on_sfx_gem_a1_finished():
	#get_node("Amanite/sfx_gem_a1").stream_paused = true
func _on_sfx_gem_b0_finished():
	get_node("Bolet00/sfx_gem_b0").stream_paused = true
func _on_sfx_gem_b1_finished():
	get_node("Bolet01/sfx_gem_b1").stream_paused = true
func _on_sfx_gem_b2_finished():
	get_node("Bolet02/sfx_gem_b2").stream_paused = true
func _on_sfx_gem_p1_finished():
	get_node("Coprin01/sfx_gem_p1").stream_paused = true
func _on_sfx_gem_x3_1_finished():
	get_node("Plsylox3_00/sfx_gem_x3_1").stream_paused = true
func _on_sfx_gem_blue_finished():
	get_node("Drop_Lantern_blue/sfx_gem_blue").stream_paused = true
#func _on_sfx_gem_orange_finished():
#	get_node("Drop_Lantern_orange/sfx_gem_orange").stream_paused = true

	#get_parent().add_child(bullet)
	#bullet = bullet.get_translation()
	#bullet.set_translation(Vector3(-2.183,0,5.072))
	#bullet.show()

# Eat animation #
func _on_Area_Drop_body_entered(_body):
	animation_crapaud.play("Eat")
	#get_parent().add_child(bullet)
	#bullet = bullet.get_translation()
	#bullet.set_translation(Vector3(-2.183,0,5.072))
	#bullet
func _on_Area_Drop2_body_entered(_body):
	animation_crapaud.play("Eat")
func _on_Area_Drop3_body_entered(_body):
	animation_crapaud.play("Eat")
func _on_Area_Drop4_body_entered(_body):
	animation_crapaud.play("Eat")
func _on_Area_Amanite_body_entered(_body):
	animation_crapaud.play("Eat")
func _on_Area_Bolet00_body_entered(_body):
	animation_crapaud.play("Eat")
func _on_Area_Bolet01_body_entered(_body):
	animation_crapaud.play("Eat")
func _on_Area_Bolet02_body_entered(_body):
	animation_crapaud.play("Eat")
func _on_Area_Coprin01_body_entered(_body):
	animation_crapaud.play("Eat")
func _on_Area_Plsylox3_00_body_entered(_body):
	animation_crapaud.play("Eat")
func _on_Area_Drop_Lantern_blue_body_entered(_body):
	animation_crapaud.play("Eat")

# Game Over panel
func _on_Area_body_entered(_body):
	var _go
	_go = get_tree().change_scene("res://menu/game_over/game_over3.tscn")
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	var go_panel = get_node("Game_over_level3")
	go_panel.show()
	pass # Replace with function body.

func _on_Menu_pause_button_play_pressed():
	var go_menu_pause = get_node("Menu_pause")
	go_menu_pause.hide()
	#Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	pass # Replace with function body.

func _on_Menu_pause_button_settings_pressed():
	var setp = get_node("settings")
	setp.show()
	pass # Replace with function body.

func _on_Game_over_level3_button_replay_pressed():
	var _gop
	_gop = get_tree().change_scene("res://level/level3.tscn")
	pass # Replace with function body.

func _on_settings_vol_pressed():
	var vol_level3 = vol.instance()
	vol_level3.get_node("music_techno").stop()
	#get_node("music").stop()
	pass # Replace with function body.

func _on_Area2_body_entered(_body):
	get_node("troll_door/AnimationPlayer").play("Troll_door")
	var door = get_node("Troll_door/CollisionShape")
	door.set_disabled(true)

	var anim_button = get_node("Button/AnimationPlayer").get_animation("Push_button")
	anim_button.set_loop(false)
	get_node("Button/AnimationPlayer").play("Push_button")
	pass # Replace with function body.

func _on_Area_Key_body_entered(_body):
	get_node("Key").hide()
	var anim_key = get_node("cave_door/AnimationPlayer").get_animation("Cave_door")
	anim_key.set_loop(false)
	get_node("cave_door/AnimationPlayer").play("Cave_door")
	var cav_door = get_node("Cave_door-col/CollisionShape")
	cav_door.set_disabled(true)
	pass # Replace with function body.

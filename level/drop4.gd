extends Spatial

onready var animation = get_node("AnimationPlayer")
onready var soundfx = get_node("sfx_gem4")
export var collision_count3 = 0 #Your counter.

func _ready():
	animation.get_animation("Drop_move_blue").set_loop(true)
	animation.play("Drop_move_blue", 1, 1, true)
	pass

func _on_Area_Drop4_body_entered(body):
	if body is KinematicBody:
		soundfx.play()
		get_node(".").hide()
		collision_count3 += 1
		#print("compteur 3 : ", collision_count3)
	pass # Replace with function body.

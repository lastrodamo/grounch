extends Spatial

# Timer
var time_start = 0
var time_now = 0

# Wait time Game Over
var elapsed_seconds = 0
var max_seconds = 1.0

# Wait time psyche
var elapsed_seconds_p = 0
var max_seconds_p = 8.0

# Eat
onready var animation_crapaud = get_node("Crapaud/crapaud/AnimationPlayer")

# Hue
onready var hue_timer = 0
onready var speed = 300 # degrees per second 60

onready var luciole = get_node("Path/PathFollow/firefly_blue")
onready var crapaud = get_node("Crapaud")

export var pos_f = Vector3()

#Texture Firefly
onready var text_orange = preload("res://prey/luciole/textures/uv_skin_orange.png")


func _ready():
	if (settings.gi_quality == settings.GI_QUALITY_HIGH):
		ProjectSettings["rendering/quality/voxel_cone_tracing/high_quality"]=true
	elif (settings.gi_quality == settings.GI_QUALITY_LOW):
		ProjectSettings["rendering/quality/voxel_cone_tracing/high_quality"]=false
	else:
		$GIProbe.hide()
		$refprobes.show()
		
	if (settings.aa_quality == settings.AA_8X):
		get_node("/root").msaa = Viewport.MSAA_8X
	elif (settings.aa_quality == settings.AA_4X):
		get_node("/root").msaa = Viewport.MSAA_4X
	elif (settings.aa_quality == settings.AA_2X):
		get_node("/root").msaa = Viewport.MSAA_2X
	else:
		get_node("/root").msaa = Viewport.MSAA_DISABLED
		
	if (settings.ssao_quality == settings.SSAO_QUALITY_HIGH):
		pass
	elif (settings.ssao_quality == settings.SSAO_QUALITY_LOW):
		pass
	else:
		$WorldEnvironment.environment.ssao_enabled = false
		
	if (settings.resolution == settings.RESOLUTION_NATIVE):
		pass
	elif (settings.ssao_quality == settings.RESOLUTION_1080):
		var minsize=Vector2( OS.window_size.x * 1080 / OS.window_size.y, 1080.0)
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,SceneTree.STRETCH_ASPECT_KEEP_HEIGHT,minsize)
	elif (settings.ssao_quality == settings.RESOLUTION_720):
		var minsize=Vector2( OS.window_size.x * 720 / OS.window_size.y, 720.0)
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,SceneTree.STRETCH_ASPECT_KEEP_HEIGHT,minsize)
	elif (settings.ssao_quality == settings.RESOLUTION_576):
		var minsize=Vector2( OS.window_size.x * 576 / OS.window_size.y, 576.0)
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,SceneTree.STRETCH_ASPECT_KEEP_HEIGHT,minsize)

# timer
	time_start = OS.get_unix_time()
	set_process(true)

# game design
	get_node("Game_design").show()
	pass

func _process(delta):
# Return to menu
	if Input.is_action_just_pressed("ui_cancel") or Input.is_action_just_pressed("p"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		var go_menu_pause = get_node("Menu_pause")
		go_menu_pause.show()

##### Game over #####
	var game_over = get_node("Crapaud")
	#var pos_init = Vector3(0,0,0)
	#print("G : ", game_over.G)

##### Timer #####
	if game_over.G <= -9.81:
		elapsed_seconds += delta
		if elapsed_seconds > max_seconds:
			elapsed_seconds = 1
			var death = get_node("Crapaud/crapaud/AnimationPlayer")
			death.get_animation("Fall").set_loop(true)
			death.play("Fall", 1, 1, true)
	pass

func _physics_process(delta):
	if Input.is_action_pressed("e") or Input.is_action_pressed("LMB"):
		pos_f = get_node("Path/PathFollow").get_translation()
		var pos_ff = Vector3(pos_f.x, 1, pos_f.z)
		#print("Translation : ", pos_ff)
		var crap = crapaud.get_node("MeshInstance")
		#var pos_c = Vector3()
		var init_pose = crapaud.get_translation()
		crap.set_translation(pos_ff - init_pose)
		#var c = get_node("Crapaud/crapaud/Armature_crapaud")
		#print(c)
		#c = c.look_at(-pos_ff, Vector3(0, 1, 0))
		#luciole.hide()
		#get_node("Path/PathFollow/OmniLight_blue").hide()
		
		
		#elapsed_seconds += delta
		#if elapsed_seconds > max_seconds:
		#	elapsed_seconds = 0
		#	c = c.look_at(Vector3(1,1,1))
		#var alpha = atan(pos_f.x/pos_f.z)
		#alpha = rad2deg(alpha)
		#print("pos_f.x : ", pos_f.x)
		#print("pos_f.z : ", pos_f.z)
		#print("angle : ", alpha)
		#c.rotate_y(alpha)
		
	
	
	
# Compteur
	var a1 = get_node("Amanite").collision_count
	var c0 = get_node("Drop").collision_count
	var c1 = get_node("Drop2").collision_count1
	var c2 = get_node("Drop3").collision_count2
	var c3 = get_node("Drop4").collision_count3
#	var cb = get_node("Drop_Lantern_blue").collision_count_blue
	var co = get_node("Drop_Lantern_orange").collision_count_orange
	var ccb0 = get_node("Bolet00").collision_count
	var ccb1 = get_node("Bolet01").collision_count
	var ccb2 = get_node("Bolet02").collision_count
	var ccc1 = get_node("Coprin01").collision_count
	var ccp2 = get_node("Plsylox3_00").collision_count
	
	var count = c0 + c1 + c2 + c3
	var ccb = ccb0 + ccb1 + ccb2
	var ccc = ccc1
	var ccp = ccp2
		
	#print("count = ", count)
	#print("ccb = ", ccb)
	#print("ccp = ", ccp)
	
#### Ouverture de la lanterne bleue
	var house_door = get_node("Level2/AnimationPlayer")
	var luciole = get_node("Path/PathFollow/firefly_blue")
	if co == 1:
		house_door = house_door.play("Open_the_door", 1, 0.3)
		luciole.get_parent().set_offset(luciole.get_parent().get_offset() + (5*delta))
		luciole.get_node("luciole/Armature/Skeleton")
		# Changement de texture de la Luciole
		var firefly_text = get_node("Path/PathFollow/firefly_blue/luciole/Armature/Skeleton/Firefly")
		#.get_surface_material("Skin.material")
		#var firefly_text = 
		firefly_text.get("albedo_texture")
		firefly_text.set("albedo_texture", text_orange)

	var gd_mauve = get_node("Game_design/ProgressBar_perle_mauve")
	var gd_bleue = get_node("Game_design/ProgressBar_perle_bleue")
	#var gd_orange = get_node("Game_design/ProgressBar_perle_orange")
	var gd_bolet = get_node("Game_design/ProgressBar_champi_bolet")
	var gd_coprin = get_node("Game_design/ProgressBar_champi_coprin")
	var gd_psylo = get_node("Game_design/ProgressBar_champi_psylo")
	
#warning-ignore:unused_variable
	var pb_m
#warning-ignore:unused_variable
	var pb_b
#warning-ignore:unused_variable
	var pb_o
#warning-ignore:unused_variable
	var pb_cb
#warning-ignore:unused_variable
	var pb_cc
#warning-ignore:unused_variable
	var pb_cp
	
	pb_m = gd_mauve.set_value(count*25)
	pb_b = gd_bleue.set_value(co*100)
	#pb_o = gd_orange.set_value(co*100)
	pb_cb = gd_bolet.set_value(ccb*33.4)
	pb_cc = gd_coprin.set_value(ccc*100)
	pb_cp = gd_psylo.set_value(ccp*100)

	var base_color = Color(0.8, 0.8, 0.8, 1.0)
	var crapaud_shading = get_node("Crapaud/crapaud/Armature_crapaud/Skeleton/Crapaud").get_mesh().surface_get_material(0)

	if c0 >= 1:
		var areac0 = get_node("Area_Drop/CollisionShape")
		areac0.set_disabled(true)
	if c1 >= 1:
		var areac1 = get_node("Area_Drop2/CollisionShape")
		areac1.set_disabled(true)
	if c2 >= 1:
		var areac2 = get_node("Area_Drop3/CollisionShape")
		areac2.set_disabled(true)
	if c3 >= 1:
		var areac3 = get_node("Area_Drop4/CollisionShape")
		areac3.set_disabled(true)
	#if cb >= 1:
	#	var areacb = get_node("Area_Drop_Lantern_blue/CollisionShape")
	#	areacb.set_disabled(true)
	if co >= 1:
		var areaco = get_node("Area_Drop_Lantern_orange/CollisionShape")
		areaco.set_disabled(true)
	if ccb0 >= 1:
		var areab0 = get_node("Area_Bolet00/CollisionShape")
		areab0.set_disabled(true)
	if ccb1 >= 1:
		var areab1 = get_node("Area_Bolet01/CollisionShape")
		areab1.set_disabled(true)
	if ccb2 >= 1:
		var areab2 = get_node("Area_Bolet02/CollisionShape")
		areab2.set_disabled(true)
	if ccc1 >= 1:
		var areap1 = get_node("Area_Coprin01/CollisionShape")
		areap1.set_disabled(true)
	if ccp2 >= 1:
		var areap2 = get_node("Area_Plsylox3_00/CollisionShape")
		areap2.set_disabled(true)

	#Simple number that goes from 0 to 360 and repeats.
		hue_timer = fmod(hue_timer + delta * speed, 360)
		var h = hue_timer / 360 #h,s,v needs to be in range 0-1
		var new_color2 = Color()
		new_color2.v = 1 #value
		new_color2.s = 1 #saturation
		new_color2.h = h #hue

		#elapsed_seconds_p = 0
		elapsed_seconds_p += delta
		if elapsed_seconds_p < max_seconds_p:
			
			new_color2 = crapaud_shading.set_albedo(new_color2)
		else:
			new_color2 = crapaud_shading.set_albedo(base_color)
			#print("Base color : ", base_color)
		
	if a1 == 1:
		var _go2
		_go2 = get_tree().change_scene("res://menu/game_over/game_over2.tscn")
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		var go_panel = get_node("Game_over_level2")
		go_panel.show()
		
	if (count == 4 and ccb == 3 and ccc == 1 and ccp == 1 and co == 1):
		get_node("Vortex").show()
		get_node("Area_vortex").show()
		get_node("Area_vortex/CollisionShape").set_disabled(false)
		#var mouse_pos = get_viewport().get_mouse_position()

func _on_sfx_gem_finished():
	get_node("Drop/sfx_gem").stream_paused = true
func _on_sfx_gem2_finished():
	get_node("Drop2/sfx_gem2").stream_paused = true
func _on_sfx_gem3_finished():
	get_node("Drop3/sfx_gem3").stream_paused = true
func _on_sfx_gem4_finished():
	get_node("Drop4/sfx_gem4").stream_paused = true
func _on_sfx_gem_a1_finished():
	get_node("Amanite/sfx_gem_a1").stream_paused = true
func _on_sfx_gem_b0_finished():
	get_node("Bolet00/sfx_gem_b0").stream_paused = true
func _on_sfx_gem_b1_finished():
	get_node("Bolet01/sfx_gem_b1").stream_paused = true
func _on_sfx_gem_b2_finished():
	get_node("Bolet02/sfx_gem_b2").stream_paused = true
func _on_sfx_gem_p1_finished():
	get_node("Coprin01/sfx_gem_p1").stream_paused = true
func _on_sfx_gem_x3_1_finished():
	get_node("Plsylox3_00/sfx_gem_x3_1").stream_paused = true
#func _on_sfx_gem_blue_finished():
#	get_node("Drop_Lantern_blue/sfx_gem_blue").stream_paused = true
func _on_sfx_gem_orange_finished():
	get_node("Drop_Lantern_orange/sfx_gem_orange").stream_paused = true

# Eat animation #
#warning-ignore:unused_argument
func _on_Area_Amanite_body_entered(_body):
	animation_crapaud.play("Eat")
func _on_Area_Bolet00_body_entered(_body):
	animation_crapaud.play("Eat")
#warning-ignore:unused_argument
func _on_Area_Bolet01_body_entered(_body):
	animation_crapaud.play("Eat")
#warning-ignore:unused_argument
func _on_Area_Bolet02_body_entered(_body):
	animation_crapaud.play("Eat")
#warning-ignore:unused_argument
func _on_Area_Coprin01_body_entered(_body):
	animation_crapaud.play("Eat")
#warning-ignore:unused_argument
func _on_Area_Plsylox3_00_body_entered(_body):
	animation_crapaud.play("Eat")
#func _on_Area_Drop_Lantern_blue_body_entered(body):
#	animation_crapaud.play("Eat")
func _on_Area_Drop_Lantern_orange_body_entered(_body):
	animation_crapaud.play("Eat")
#warning-ignore:unused_argument
func _on_Area_Drop_body_entered(_body):
	animation_crapaud.play("Eat")
#warning-ignore:unused_argument
func _on_Area_Drop2_body_entered(_body):
	animation_crapaud.play("Eat")
#warning-ignore:unused_argument
func _on_Area_Drop3_body_entered(_body):
	animation_crapaud.play("Eat")
#warning-ignore:unused_argument
func _on_Area_Drop4_body_entered(_body):
	animation_crapaud.play("Eat")


func _on_Area_vortex_body_entered(body):
	if body is KinematicBody:
#warning-ignore:return_value_discarded
		get_tree().change_scene("res://level/panel_levels/panel_level2.tscn")
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	pass # Replace with function body.

func _on_Game_over_level2_button_replay_pressed():
	var _go_level2
	_go_level2 = get_tree().change_scene("res://level/level2.tscn")
	pass # Replace with function body.

func _on_Area_Game_over_body_entered(body):
	var _go
	_go = get_tree().change_scene("res://menu/game_over/game_over2.tscn")
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	var go_panel = get_node("Game_over_level2")
	go_panel.show()
	pass # Replace with function body.


func _on_Menu_pause_button_play_pressed():
	var go_menu_pause = get_node("Menu_pause")
	go_menu_pause.hide()
	pass # Replace with function body.


func _on_Menu_pause_button_settings_pressed():
	var setp = get_node("settings")
	setp.show()
	pass # Replace with function body.


func _on_settings_vol_pressed():
	get_node("music").stop()
	pass # Replace with function body.


extends Spatial

onready var animation = get_node("AnimationPlayer")
onready var soundfx = get_node("sfx_gem2")
export var collision_count1 = 0 #Your counter.

func _ready():
	animation.get_animation("Drop_move_blue").set_loop(true)
	animation.play("Drop_move_blue", 1, 1, true)
	pass

func _on_Area_Drop2_body_entered(body):
	if body is KinematicBody:
		soundfx.play()
		get_node(".").hide()
		collision_count1 += 1
	pass # Replace with function body.


func _on_Area_Drop2_body_exited(_body):
	pass # Replace with function body.

extends Spatial

func _ready():
	
	pass

func _input(_event):
	var pos1 = get_translation()
	var cam1 = get_tree().get_root().get_camera()
	var screen_pos1 = cam1.unproject_position(pos1)
	screen_pos1.x = round(screen_pos1.x)
	screen_pos1.y = round(screen_pos1.y)
	var bn = get_node("TextureButton")
	#var tn = bn.get_position()
	bn.set_position(Vector2(screen_pos1.x, screen_pos1.y))
	pass

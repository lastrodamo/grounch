extends KinematicBody

# Cible
var aiming = false

### Gravity
const GRAVITY = Vector3(0,-9.8, 0)

# Interpolate
const DIRECTION_INTERPOLATE_SPEED = 1
const MOTION_INTERPOLATE_SPEED = 10
const ROTATION_INTERPOLATE_SPEED = 10
var motion = Vector2()

### Orbit
const CAMERA_ROTATION_SPEED = 0.001
const CAMERA_X_ROT_MIN = -40
const CAMERA_X_ROT_MAX = 30
var orientation = Transform()
var camera_x_rot = 0.0

### Frog displacement
var velocity = Vector3()
var speed = 0.64
var root_motion = Transform()

### Jump
var airborne_time = 250 # 150 2.91
const MIN_AIRBORNE_TIME = 0.01
const JUMP_SPEED = 5.8

### Frog gravity
export var G = 0.00

### Frog animation
onready var animation = get_node("crapaud/AnimationPlayer")

func _input(event):
	if event is InputEventMouseMotion:
		$camera_base.rotate_y( -event.relative.x * CAMERA_ROTATION_SPEED )
		$camera_base.orthonormalize() # after relative transforms, camera needs to be renormalized
		camera_x_rot = clamp(camera_x_rot - event.relative.y * CAMERA_ROTATION_SPEED,deg2rad(CAMERA_X_ROT_MIN), deg2rad(CAMERA_X_ROT_MAX) )
		$camera_base/camera_rot.rotation.x =  camera_x_rot

func _ready():
	#pre initialize orientation transform	
	orientation=$"crapaud".global_transform
	orientation.origin = Vector3()
#	camera_base/camera_rot/Camera.add_exception(self)

func _physics_process(delta):
	var motion_target = Vector2( 	Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
									Input.get_action_strength("move_forward") - Input.get_action_strength("move_back") )

	motion = motion.linear_interpolate(motion_target, MOTION_INTERPOLATE_SPEED * delta)
	
	var cam_z = - $camera_base/camera_rot/Camera.global_transform.basis.z
	var cam_x = $camera_base/camera_rot/Camera.global_transform.basis.x

	cam_z.y=0
	cam_z = cam_z.normalized()
	cam_x.y=0
	cam_x = cam_x.normalized()
	
	var current_aim = Input.is_action_pressed("RMB")

### Cible
	if (aiming != current_aim):
		aiming = current_aim
		if (aiming):
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
### Jump/Air logic
	airborne_time += delta
	if (is_on_floor()):
#		if (airborne_time > 0):
#			### Frog jump sound
#			$sfx/land.play()
		airborne_time = 0

	var on_air = airborne_time > MIN_AIRBORNE_TIME

### Jump speed
	if (not on_air and Input.is_action_just_pressed("ui_select")):
		velocity.y = JUMP_SPEED
		on_air = true
		$animation_tree["parameters/state/current"]=2
		$sfx/jump.play()

	if (on_air):
		if (velocity.y > 0):
			$animation_tree["parameters/state/current"]=2
		else:
			$animation_tree["parameters/state/current"]=3
			$sfx/jump.stop()
	
	#if Input.is_action_just_pressed("ui_select"):
	#	velocity.y = JUMP_SPEED
	#	on_air = true
	#	var anim = get_node("Scene Root/AnimationPlayer")
	#	anim.get_animation("Jump").set_loop(true)
	#	anim.play("Jump", 1, 1, true)
	#	$animation_tree["parameters/state/current"]=6
	#	$sfx/jump.play()
	
	else: 		
		# convert orientation to quaternions for interpolating rotation

		var idle = animation.get_animation("Idle").set_loop(true)

		var target = - cam_x * motion.x -  cam_z * motion.y
		if (target.length() > 0.001):
			var q_from = Quat(orientation.basis)
			var q_to = Quat(Transform().looking_at(target,Vector3(0,1,0)).basis)
	
			# interpolate current rotation with desired one
			orientation.basis = Basis(q_from.slerp(q_to,delta*ROTATION_INTERPOLATE_SPEED))

### Change state (Idle) to walk

		$animation_tree["parameters/state/current"]=1

		# play walk with loop
		if Input.is_action_pressed("shift"):
			animation.get_animation("Run").set_loop(true)
		else:
			animation.get_animation("Walk_front").set_loop(true)
		# blend position for walk speed based on motion
		$animation_tree["parameters/walk/blend_position"]=Vector2(motion.length(), 0) 

		if Input.is_action_pressed("ctrl"):
			$animation_tree["parameters/state/current"]=4
	
		# get root motion transform
		root_motion = $animation_tree.get_root_motion_transform()
	
	# apply root motion to orientation
	orientation *= root_motion

	if Input.is_action_pressed("shift"):
		var h_velocity = orientation.origin / (delta * speed * 0.75)
		velocity.x = h_velocity.x
		velocity.z = h_velocity.z
	else:
		var h_velocity = orientation.origin / (delta * speed)
		velocity.x = h_velocity.x
		velocity.z = h_velocity.z

	velocity += GRAVITY * delta
	#velocity.y += GRAVITY * delta
	move_and_slide(velocity, Vector3(0,1,0), false, 4, 0.9)
	
	orientation.origin = Vector3() #clear accumulated root motion displacement (was applied to speed)
	orientation = orientation.orthonormalized() # orthonormalize orientation
	
	$"crapaud".global_transform.basis = orientation.basis

func _init():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

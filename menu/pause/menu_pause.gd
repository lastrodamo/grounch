extends Control

signal button_play_pressed
signal button_settings_pressed

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_TextureButton_Play_button_down():
	#get_tree().change_scene("res://level/level.tscn")
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	pass # Replace with function body.

func _on_TextureButton_Quit_button_down():
	var _qd
	_qd = get_tree().change_scene("res://menu/menu.tscn")
	pass # Replace with function body.



func _on_TextureButton_Play_pressed():
	emit_signal("button_play_pressed")
	pass # Replace with function body.


func _on_TextureButton_Settings_pressed():
	emit_signal("button_settings_pressed")
	pass # Replace with function body.

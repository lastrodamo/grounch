extends Control

signal button_replay_pressed

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_TextureButton_Replay_button_down():
	emit_signal("button_replay_pressed")
	var _re_bd
	_re_bd = get_tree().change_scene("res://level/level2.tscn")
	pass # Replace with function body.

func _on_TextureButton_Quit_button_down():
	var _qbd
	_qbd = get_tree().change_scene("res://menu/menu.tscn")
	pass # Replace with function body.

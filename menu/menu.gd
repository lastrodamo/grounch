extends Spatial

var viewport_scale
var current_scene = null

func _ready():
	#var animation = get_node("crapaud/AnimationPlayer")
	#animation.play("Idle", 1, 1, true)
	pass

func _on_play_pressed():
	$ui/main.hide()
	$ui/loading.show()
	$begin_load_timer.start()

func _on_settings_pressed():
	var setting = get_node("settings")
	setting.show()
	pass

func _on_help_pressed():
	var help = get_node("Help")
	help.show()
	pass # Replace with function body.

func _on_Close_button2_pressed():
	var help = get_node("Help")
	help.hide()
	pass # Replace with function body.

func _on_sound_toggled(button_pressed):
	if button_pressed == true:
		var sound = get_node("music")
		sound.stop()
	else:
		var sound = get_node("music")
		sound.play()
	pass # Replace with function body.

func resolution():
	var viewport = get_viewport().get_visible_rect().size
	viewport_scale = viewport.x/viewport.y
	pass

func _input(_event):
# Return to menu Help
	var help = get_node("Help")
	if Input.is_action_pressed("ui_cancel"):
		help.hide()
# Quit
	if Input.is_action_pressed("q"):
		get_tree().quit()
	pass
# Return to menu Settings
	var set = get_node("settings")
	if Input.is_action_pressed("ui_cancel"):
		set.hide()

func _on_quit_pressed():
	get_tree().quit()
#	pass # Replace with function body.

func _on_apply_pressed():
	$ui/main.show()
	$ui/settings.hide()
	
	if($ui/settings/gi_high.pressed):
		settings.gi_quality = settings.GI_QUALITY_HIGH
	elif($ui/settings/gi_low.pressed):
		settings.gi_quality = settings.GI_QUALITY_LOW
	elif($ui/settings/gi_disabled.pressed):
		settings.gi_quality = settings.GI_QUALITY_DISABLED

	if($ui/settings/aa_8x.pressed):
		settings.aa_quality = settings.AA_8X
	elif($ui/settings/aa_4x.pressed):
		settings.aa_quality = settings.AA_4X
	elif($ui/settings/aa_2x.pressed):
		settings.aa_quality = settings.AA_2X
	elif($ui/settings/aa_disabled.pressed):
		settings.aa_quality = settings.AA_DISABLED

	if($ui/settings/ssao_high.pressed):
		settings.ssao_quality = settings.SSAO_QUALITY_HIGH
	elif($ui/settings/ssao_low.pressed):
		settings.ssao_quality = settings.SSAO_QUALITY_LOW
	elif($ui/settings/ssao_disabled.pressed):
		settings.ssao_quality = settings.SSAO_QUALITY_DISABLED

# resize
	if($ui/settings/resolution_native.pressed):
		settings.resolution = settings.RESOLUTION_NATIVE
	elif($ui/settings/resolution_1080.pressed):
		settings.resolution = settings.RESOLUTION_1080
	elif($ui/settings/resolution_720.pressed):
		settings.resolution = settings.RESOLUTION_720
	elif($ui/settings/resolution_576.pressed):
		settings.resolution = settings.RESOLUTION_576

	settings.save_settings()

func _on_cancel_pressed():
	$ui/main.show()
	$ui/settings.hide()


func _on_begin_load_timer_timeout():
	var _timeout
	_timeout = get_tree().change_scene("res://level/level1.tscn")

### Resolutions ###
func _on_settings_bouton_fullscreen_toggle_on():
	var root = get_tree().get_root()
	root.connect("size_changed", self, "resize")
	OS.set_window_fullscreen(true)
	pass # Replace with function body.

func _on_settings_bouton_fullscreen_toggle_off():
	var root = get_tree().get_root()
	root.connect("size_changed", self, "resize")
	OS.set_window_fullscreen(false)
	pass # Replace with function body.

func _on_settings_bouton_576_on():
### Change resolution to 1024x576 pixels
#	var sx = get_tree().get_root().get_visible_rect().size.x
#	var sy = get_tree().get_root().get_visible_rect().size.y
#	var viewport = get_viewport().get_visible_rect().size
	var v576 = Vector2(1024, 576)
	OS.set_window_size(v576)
	pass # Replace with function body.

func _on_settings_bouton_720_on():
	var v720 = Vector2(1280, 720)
	OS.set_window_size(v720)
	pass # Replace with function body.

func _on_settings_bouton_1080_on():
	var v1080 = Vector2(1920, 1080)
	OS.set_window_size(v1080)
	pass # Replace with function body.

func _on_settings_bouton_native_on():
	var vnative = Vector2(1280, 720)
	OS.set_window_size(vnative)
	pass # Replace with function body.

func _on_settings_vol_pressed():
	#var vol_level3 = vol.instance()
	#vol_level3.get_node("music_techno").stop()
	pass # Replace with function body.

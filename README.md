# Grounch

Grounch is plateform game based on [TPS demo](https://github.com/godotengine/tps-demo) game

Developped with [Godot Engine 3.2.2](https://godotengine.org)  & blender 2.83 LTS

<img src="vignette_grounch.jpg" alt="Screenshot of Grounch" title="Screenshot of Grounch" width="720" height="400" />

<!-- <img src="doc/crapaud-15.png" alt="Screenshot of Grounch" title="Screenshot of Grounch" width="720" height="400" /> -->

## About the game

Grounch is a 3D plateform game. You play the role of a frog named Grounch. The aim is to free fireflies and find and eat differents objects as pearls and mushrooms that you will need to eat in order to get to the level. Attention There is some traps.

<img src="doc/eating.png" alt="Food" title="Food" width="601" height="333" />

There will 6 levels. At this moment 2 are ready. The third is in development

Here the characters of the game

<img src="doc/personnages_560px.jpg" alt="Fireflies" title="Fireflies" width="560" />

## Minimal requirements

<ul>
<li>150 Mo minimum of Disk space</li>
<li>Minimum RAM memory : 1024Mo</li>
<li>CG with 2G of RAM mini</li>
</ul>

## Install

GNU/Linux : Extract the file tar.gz et launch the binary file grounch_0.2.x86_64

Windows : Extract the file zip et launch the binary file grounch_0.2.exe
Licence

## License

See [LICENSE.txt](/LICENSE.txt) for details.
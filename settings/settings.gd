extends Control

### Fullscreen ###
signal bouton_fullscreen_toggle_on
signal bouton_fullscreen_toggle_off
signal bouton_576_on
signal bouton_720_on
signal bouton_1080_on
signal bouton_native_on

### Volume ###
signal vol_pressed

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_cancel_pressed():
	var settings_root = get_node(".")
	settings_root.hide()
	pass # Replace with function body.

func _on_resolution_full_toggled(button_pressed):
	if button_pressed == true:
		emit_signal("bouton_fullscreen_toggle_on")
	if button_pressed == false:
		emit_signal("bouton_fullscreen_toggle_off")
	pass # Replace with function body.

func _on_CheckBox_pressed():
	emit_signal("vol_pressed")
	pass # Replace with function body.

func _on_resolution_576_pressed():
	emit_signal("bouton_576_on")
	pass # Replace with function body.

func _on_resolution_720_pressed():
	emit_signal("bouton_720_on")
	pass # Replace with function body.

func _on_resolution_1080_pressed():
	emit_signal("bouton_1080_on")
	pass # Replace with function body.

func _on_resolution_native_pressed():
	emit_signal("bouton_native_on")
	pass # Replace with function body.
